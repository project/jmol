<?php

/**
 * @file
 * Install messages and library checking for the Jmol module.
 */

/**
 * Implements hook_install().
 */
function jmol_install() {
  $requirements = jmol_requirements('install');
  $message = $requirements['jmol']['value'];
  if ($requirements['jmol']['severity'] == 2) {
    $messagetype = 'error';
  }
  else {
    $messagetype = 'status';
  }
  drupal_set_message($message, $messagetype);
}

/**
 * Implements hook_requirements().
 */
function jmol_requirements($phase) {
  $requirements = array();

  // Ensure translations don't break at install time.
  $t = get_t();

  drupal_load('module', 'libraries');
  $directory = libraries_get_path('jmol');

  // Check the exisence of the Jmol Library Jmol.js file.
  if (($phase == 'install') or ($phase == 'runtime')) {
    $errors = array();

    if (!file_exists($directory . '/Jmol.js')) {
      $errors[] = $t('The file %file is not present, indicating that the Jmol library is not correctly installed.', array('%file' => 'Jmol.js', '%directory' => $directory));
    }

    $requirements['jmol'] = array(
      'title' => $t('Jmol'),
      'value' => !empty($errors) ? theme('item_list', array('items' => $errors)) . $t('Please consult README.txt for installation instructions.') : $t('Jmol has been installed correctly. You are ready to go.'),
      'severity' => !empty($errors) ? REQUIREMENT_ERROR : REQUIREMENT_OK,
      // Adding a description key suppresses an error during install,
      // see @link http://drupal.org/node/1312636 @endlink.
      'description' => '',
    );
  }
  return $requirements;
}
